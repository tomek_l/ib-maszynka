import React from 'react';
import Header from '../../components/Header/Header';
import './index.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from '../../views/Login/Login'

class Root extends React.Component {
  render (){
    return (
      <BrowserRouter>
        { window.location.pathname!=='/login' && <Header />}
        <Switch>
          <Route exact path="/" />
          <Route exact path="/login" component={Login} />
          {/* <Route exact path="/join" component={JoinUs}/> */}
        </Switch>
      </BrowserRouter>
    )
  }
};

export default Root;