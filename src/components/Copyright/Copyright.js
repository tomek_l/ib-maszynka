import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';


const website = 'https://race.laczek.com'

const Copyright = () => (
    <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href={website}>
            Race Master
    </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
    </Typography>
)

export default Copyright;