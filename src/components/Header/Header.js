import React from 'react';
import styles from './Header.module.scss';
import HeaderNavigation from './HeaderNavigation';

const Header = () => (
    <header className={styles.wrapper}>
        <img src="" alt=""/>
        <HeaderNavigation />
        <button className={styles.button}>Logowanie</button>        
    </header>
);

export default Header;